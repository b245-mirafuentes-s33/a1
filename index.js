fetch("https://jsonplaceholder.typicode.com/todos", {method: 'GET'})
.then(response => response.json())
.then(result => console.log(result))

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(result =>{
	let title = result.map(element => element.title)
	console.log(title)
})

fetch("https://jsonplaceholder.typicode.com/todos/1", {method: 'GET'})
.then(response => response.json())
.then(result => {
	console.log(`The item "delectus aut autem" has a status of false`)
	console.log(result)
})

fetch("https://jsonplaceholder.typicode.com/todos", {
		method: 'POST',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Create to do things',
			completed: false,
			userId: 1
		})
})
.then(response => response.json())
.then(result => console.log(result))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PUT',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated to do things',
			description: 'To update the my to do list with a different data structure',
			status: "Pending",
			dateCompleted: "Pending",
			userId: 1
		})
})
.then(response => response.json())
.then(result => console.log(result))

fetch("https://jsonplaceholder.typicode.com/todos/1", {
		method: 'PATCH',
		headers: {
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			status: "Complete",
			dateCompleted: "07/09/21",
		})
})
.then(response => response.json())
.then(result => console.log(result))

fetch("https://jsonplaceholder.typicode.com/todos/1", {method: 'DELETE'})
.then(response => response.json())
.then(result => console.log(result))

